## Hello there!

<img src="https://gitlab.com/lucablackwell/lucablackwell/-/raw/main/hello.gif" width="240" height="104"/>

######  (_General Kenobi_)

I'm Luca Blackwell!

What I work with:

- PHP (Laravel)
- Blade
- Markdown

What I've dabbled in:
- Bash (short scripts for swapping containers around)
- Python (my first language, used for Computer Science at GCSE)
- Go

What I'm currently working on: 
- Contributing where I can to [Onelinerhub](https://onelinerhub.com/)

Links:
- :cherry_blossom: [**my commits of the last year in 3D**](https://skyline.github.com/lucablackwell/2022) :cherry_blossom:
- :dragon: [**bash script to swap between local docker-compose instances**](https://gist.github.com/lucablackwell/d26be4ac9b5f245cda9b7a65b330eeda) :dragon:
- :cherry_blossom: [**one-line or interactive CLI 'password-ifier'**](https://gist.github.com/lucablackwell/46fbe70e84fab033fbbd67505518bcca) :cherry_blossom:
- :dragon: [**interactive CLI text adventure based on the game 'eXit' from the series Mr Robot—with a 'secret' mode**](https://github.com/lucablackwell/eXit) :dragon:
- :cherry_blossom: [**interactive CLI experience of my longest poem to date (being adapted into an rpgmaker game)**](https://gist.github.com/lucablackwell/d021e6fa7d53cc38f8f06591c32fa093) :cherry_blossom:
- :dragon: [**CLI-ification of my favourite childhood boardgame, Lego Heroica**](https://github.com/lucablackwell/heroica) :dragon:
- :cherry_blossom: [**my (public) gists**](https://gist.github.com/lucablackwell) :cherry_blossom:
- :dragon: [**shameless poetry collection self-promotion**](https://www.amazon.co.uk/Dead-House-Collection-Luca-Blackwell/dp/B09NVLQV38/) :dragon:
